package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Categoria;

/**
 *
 * @author ronald.reyesusam
 */
public class CategoriaDao {
    Conexion conn;
    
    public CategoriaDao(Conexion conn) {
        this.conn = conn;
    }
    
    public boolean insertar(Categoria cat){
        String sql = "INSERT INTO categoria VALUE(?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, cat.getId_categoria());
            ps.setString(2, cat.getNombre());
            ps.setString(3, cat.getDescripcion());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean actualizar(Categoria cat){
        String sql = "UPDATE categoria SET nombre =?, descripcion =? WHERE id_categoria =?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, cat.getNombre());
            ps.setString(2, cat.getDescripcion());
            ps.setInt(3, cat.getId_categoria());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean eliminar(int id){
        String sql = "DELETE FROM categoria WHERE id_categoria =?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(3,id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public List<Categoria> findAll(){
        String sql = "SELECT * FROM categoria";
        try {
            System.out.println(conn);
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Categoria> categorias = new LinkedList<>();
            while(rs.next()){
                Categoria catb = new Categoria(rs.getInt("id_categoria"));
                catb.setNombre(rs.getString("nombre"));
                catb.setDescripcion(rs.getString("descripcion"));
                categorias.add(catb);
            }
            return categorias;
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Categoria> findById(int id){
        String sql = "SELECT * FROM categoria WHERE id_categoria =?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<Categoria> categorias = new LinkedList<>();
            while(rs.next()){
                Categoria catb = new Categoria(rs.getInt("id_categoria"));
                catb.setNombre(rs.getString("nombre"));
                catb.setDescripcion(rs.getString("descripcion"));
                categorias.add(catb);
            }
            return categorias;
        } catch (Exception e) {
            return null;
        }
    }
}
