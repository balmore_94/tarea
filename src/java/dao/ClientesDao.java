package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Cliente;

/**
 *
 * @author ronald.reyesusam
 */
public class ClientesDao {
    Conexion conn = new Conexion();

    public ClientesDao(Conexion conn) {
        this.conn = conn;
    }
    
    public List<Cliente> findAll(){
        String sql = "SELECT * FROM cliente";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Cliente> clientes = new LinkedList<>();
            while(rs.next()){
                Cliente c = new Cliente(rs.getInt("id_cliente"));
                c.setNombre(rs.getString("nombre"));
                c.setApellido(rs.getString("apellido"));
                c.setDireccion(rs.getString("direccion"));
                c.setFecha_nacimiento(rs.getDate("fecha_nacimiento"));
                c.setTelefono(rs.getString("telefono"));
                c.setEmail(rs.getString("email"));
                clientes.add(c);
            }
            return clientes;
        } catch (Exception e) {
            return null;
        }
    }
}
