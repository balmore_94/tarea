/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.ClientesDao;
import dao.ProductoDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cliente;
import modelo.Producto;

/**
 *
 * @author ronald.reyesusam
 */
public class PrincipalServlet extends HttpServlet {
    Conexion conn = new Conexion();
    ProductoDao pr = new ProductoDao(conn);
    ClientesDao cl = new ClientesDao(conn);
    RequestDispatcher rd;
    boolean resultado;
    String msj;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch(action){
            case "clientes":
                clientes(request, response);
                break;
            case "productos":
                productos(request, response);
                break;
        }
    }
    protected void productos(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Producto>productos = pr.findAll();
        
        request.setAttribute("productos", productos);
        rd = request.getRequestDispatcher("productos.jsp");
        rd.forward(request, response);
    }
    
    protected void clientes(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Cliente>clientes = cl.findAll();
        
        request.setAttribute("clientes", clientes);
        rd = request.getRequestDispatcher("clientes.jsp");
        rd.forward(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
}
