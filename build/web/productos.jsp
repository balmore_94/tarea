<%-- 
    Document   : index
    Created on : 10-17-2019, 04:27:09 PM
    Author     : ronald.reyesusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Hola</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.jsp">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="principal?action=clientes">Cliente</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="principal?action=productos">Productos</a>
                </li>
                
            </ul>
        </nav>
        <div class="container">
            <h2>Productos</h2>

            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Stock</th>
                        <th>Categoria</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${productos}" var="ver">
                        <tr>
                            <td>${ver.id_producto}</td>
                            <td>${ver.nombre}</td>
                            <td>${ver.precio}</td>
                            <td>${ver.stock}</td>
                            <td>${ver.id_categoria.nombre}</td>
                        </tr>
                    </c:forEach>
                        
                </tbody>
            </table>
        </div>
    </body>
</html>

