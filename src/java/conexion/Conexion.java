package conexion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author ronald.reyesusam
 */
public class Conexion {
    static String user = "root";
    static String pass = "root";
    static String bd = "participante7";
    static String url = "jdbc:mysql://localhost/" + bd + "?useSSL=false"; 
    Connection conn = null;

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            if (conn != null) {
                System.out.println("Conexion exitosa");
            }
        } catch (Exception e) {
            System.out.println("Error");
        }
    
    }
    
    public Connection conectar(){
        return conn;
    }
   
   
}
