package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Categoria;
import modelo.Producto;

/**
 *
 * @author ronald.reyesusam
 */
public class ProductoDao {
    Conexion conn;

    public ProductoDao(Conexion conn) {
        this.conn = conn;
    }
    
    public boolean insertar(Producto pr){
        String sql = "INSERT INTO producto VALUE(?,?,?,?,?)";
        Categoria cat = pr.getId_categoria();
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, pr.getId_producto());
            ps.setString(2, pr.getNombre());
            ps.setDouble(3, pr.getPrecio());
            ps.setInt(4, pr.getStock());
            ps.setInt(5, cat.getId_categoria());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean actualizar(Producto pr){
        String sql = "UPDATE producto SET nombre =?, precio=?, stock=?, id_categoria=? WHERE id_producto =?";
        Categoria cat = pr.getId_categoria();
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, pr.getNombre());
            ps.setDouble(2, pr.getPrecio());
            ps.setInt(3, pr.getStock());
            ps.setInt(4, cat.getId_categoria());
            ps.setInt(5, pr.getId_producto());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean eliminar(int id){
        String sql = "DELETE FROM producto WHERE id_producto =?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(5, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public List<Producto> findAll(){
        String sql = "SELECT producto.*, categoria.nombre as 'cate' FROM producto, categoria WHERE producto.id_categoria = categoria.id_categoria;";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Producto> productos = new LinkedList<>();
            while(rs.next()){
                Producto pr = new Producto(rs.getInt("id_producto"));
                pr.setNombre(rs.getString("nombre"));
                pr.setPrecio(rs.getDouble("precio"));
                pr.setStock(rs.getInt("stock"));
                Categoria c = new Categoria(rs.getInt("id_categoria"));
                c.setNombre(rs.getString("cate"));
                pr.setId_categoria(c);
                productos.add(pr);
            }
            return productos;
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Producto> findById(int id){
        String sql = "SELECT * FROM producto WHERE id_producto =?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<Producto> productos = new LinkedList<>();
            while(rs.next()){
                Producto pr = new Producto(rs.getInt("id_producto"));
                pr.setNombre(rs.getString("nombre"));
                pr.setPrecio(rs.getDouble("precio"));
                pr.setStock(rs.getInt("stock"));
                Categoria c = new Categoria(rs.getInt("id_categoria"));
                pr.setId_categoria(c);
                productos.add(pr);
            }
            return productos;
        } catch (Exception e) {
            return null;
        }
    }
}
